/**
Input: Objekt
Output: Boolen
Funktion: Das Pr�dikat sagt aus, ob das �bergebene Objekt eine Liste ist.
Beschreibung des Angorithmus: Section 1.1.1 im Entwurf.
*/
is_a_list([]).
is_a_list([_|T]) :-
        is_a_list(T).

/**
Input: Ursprungsliste, Abzugsliste
Output: Ergebnisliste
Funktion: Das Pr�dikat zieht die Elemenete der Abzugsliste von der Ursprungsliste ab und gibt die Ergebnisliste aus.
Beschreibung des Angorithmus: Section 1.1.2 im Entwurf.
*/
diffList([],_,[]):-!.
diffList([K1|R1],L2,L3):- member(K1,L2), !, diffList(R1,L2,L3).
diffList([H1|R1],L2,[H1|R3]):- diffList(R1,L2,R3).

/**
Input: Infix, Liste
Output: Boolen
Funktion: Das Pr�dikat �berpr�ft ob der Infix in der Liste enthalten ist.
Beschreibung des Angorithmus: Section 1.1.3 im Entwurf.
*/
infix2([K|[]],[K|R]):- R \== [].
infix2([K1|R1],[K1|R2]):- infix2(R1,R2).

infix([K1|R1],[_|[K1|R2]]):- infix2(R1,R2).
infix(I,[_|R2]):- I \== [], infix(I,R2).

/**
Input: Suffix, Liste
Output: Boolen
Funktion: Das Pr�dikat �berpr�ft ob der Suffix am Ende der Liste enthalten ist.
Beschreibung des Angorithmus: Section 1.1.4 im Entwurf.
*/
suffix(S,[_|S]).
suffix(S,[_|R]):- S \== [], suffix(S,R).

/**
Input: Pr�fix, Liste
Output: Boolen
Funktion: Das Pr�dikat �berpr�ft ob der Pr�fix am Anfang der Liste enthalten ist.
Beschreibung des Angorithmus: Section 1.1.5 im Entwurf.
*/
praefix(P,L):- append(P,X,L),X \== [], L \== [], P \== [].

/**
Input: Liste
Output: Even, Odd
Funktion: Das Pr�dikat z�hlt die Anzahl der Elemente in der Liste, auch die Elemente der Liste, wenn die Liste ein Element aus der Liste ist.
Beschreibung des Angorithmus: Section 1.1.6 im Entwurf.
*/
eo_count(L1,Even,Odd):- eo_count_even_odd(L1,Even1,Odd1), eo_count_unterliste(L1,LE,LO),
                        zusammenrechung(LE, Even2), zusammenrechung(LO, Odd2),
                        Even is (Even1 + Even2), Odd is (Odd1 + Odd2).
eo_count_unterliste([],[],[]).

eo_count_unterliste([H1|R1],[Even1|LE],[Odd1|LO]):- is_a_list(H1), !,
                                                    eo_count(H1,Even1,Odd1),
                                                    eo_count_unterliste(R1,LE,LO).
eo_count_unterliste([_|R1],LE,LO):- eo_count_unterliste(R1,LE,LO).

eo_count_even_odd(L1,Even,Odd):- length(L1,W), not(0 is mod(W,2)), !,
                                 Odd is 1, Even is 0.
eo_count_even_odd(L1,Even,Odd):- length(L1,W), 0 is mod(W,2), !,
                                 Even is 1, Odd is 0.

/**
Input: l/e/a, Element, Liste
Output: Ergebnissliste
Funktion: Das Pr�dikat l�scht ein bestimmtes Element aus einer Liste und gibt die �berarbeitete Liste zur�ck.
Beschreibung des Angorithmus: Section 1.1.7 im Entwurf.
*/
del_element(l,E,L1,R2):- liste_drehen(L1,L2,[]), del_element(e,E,L2,R1),
                         liste_drehen(R1,R2,[]).

del_element(e,E,[E|R2],R):- R = R2.
del_element(e,E,[H1|R1],R):- del_element(e,E,R1,R2), append(H1,R2,R).

del_element(a,_,[],[]).
del_element(a,E,[E|R1],R):- del_element(a,E,R1,R).
del_element(a,E,[H1|R1],[H1|R]):- H1 \== E, del_element(a,E,R1,R).

/**
Input: l/e/a, Element, Neues Element, Liste
Output: Ergebnissliste
Funktion: Das Pr�dikat ersetzt ein bestimmtes Element gegen ein Neues aus einer Liste und gibt die �berarbeitete Liste zur�ck.
Beschreibung des Angorithmus: Section 1.1.8 im Entwurf.
*/
substitute(l,E,E2,L1,R2):- liste_drehen(L1,L2,[]), substitute(e,E,E2,L2,R1),
                           liste_drehen(R1,R2,[]).


substitute(e,E,E2,[E|R2],[E2|R2]).
substitute(e,E,E2,[H1|R1],R):- substitute(e,E,E2,R1,R2), append(H1,R2,R).

substitute(a,_,_,[],[]).
substitute(a,E,E2,[E|R1],[E2|R2]):- substitute(a,E,E2,R1,R2).
substitute(a,E,E2,[H1|R1],[H1|R2]):- H1 \== E, substitute(a,E,E2,R1,R2).

/**
Hilfspr�dikate
*/
liste_drehen([],L1,L1).
liste_drehen([H|T],L1,L2):- liste_drehen(T,L1,[H|L2]).

zusammenrechung([],0).
zusammenrechung([KOPF|REST],Y):-
zusammenrechung(REST,X), Y is (KOPF + X).


/**
Input: X
Output: true oder false.
Funktion: Rekursive Definition der nat�rlichen Zahlen.
Beschreibung des Angorithmus: %
*/

nat(0).
nat(s(X)):-
           nat(X).

/**
Input: Zahl
Output: SZahl
Funktion: Das Pr�dikat liefert uns eine SZahl die von einer nat�rlichen Zahl erzeugt worden ist.
Beschreibung des Angorithmus: Section 1.2.1 im Entwurf.
*/

nat2s(0,0):- !.

nat2s(Zahl,s(SZahl)):-
                      Y = Zahl,
                      A is Y - 1,
                      nat2s(A,SZahl).
/**
Input: SZahl
Output: Zahl
Funktion: Das Pr�dikat liefert uns eine Zahl die von einer SZahl erzeugt worden ist.
Beschreibung des Angorithmus: Section 1.2.2 im Entwurf.
*/

s2nat(0,0):- !.

s2nat(s(SZahl),Zahl):-
                      s2nat(SZahl,A),
                      Zahl is A + 1.
/**
Input: Summand1 und Summand2
Output: Summe
Funktion: Das Pr�dikat liefert uns eine SZahl, die SZahl ist die Summe der zwei eingegebenen SZahlen.
Beschreibung des Angorithmus: Section 1.2.3 im Entwurf.
*/

add(Summand1,0,Summand1):- !.
add(0,Summand2,Summand2):- !.

add(s(Summand1),Summand2,Summe):-
                   add(Summand1,s(Summand2),Summe).
/**
Input: Minuend und Subtrahend
Output: Differenz
Funktion: Das Pr�dikat liefert eine SZahl, die SZahl ist die Differenz der zwei eingegebenen SZahlen.
Beschreibung des Angorithmus: Section 1.2.4 im Entwurf.
*/

sub(0,_,0):- !.
sub(Minuend,0,Minuend):- !.

sub(s(Minuend), s(Subtrahend), Differenz):-
                    sub(Minuend,Subtrahend,Differenz).

/**
Input: Faktor1 und Faktor2
Output: Produkt
Funktion: Das Pr�dikat liefert eine SZahl, die SZahl ist ein Produkt der Multiplikation der zwei eingegebenen SZahlen.
Beschreibung des Angorithmus: Section 1.2.5 im Entwurf.
*/

mul(0,_,0):- !.
mul(_,0,0):- !.

mul(s(0),Faktor2,Faktor2):- !.
mul(Faktor1,s(0),Faktor1):- !.

mul(s(Faktor1),Faktor2,Produkt):-
                                mul(Faktor1,Faktor2,Summe),
                                add(Summe,Faktor2,Produkt),!.

/**
Input: B und E
Output: R
Funktion: Das Pr�dikat liefert eine SZahl, die SZahl ist ein Produkt der Exponention der zwei eingegebenen SZahlen.
Beschreibung des Angorithmus: Section 1.2.6 im Entwurf.
*/

power(_,0,s(0)).
power(B,s(0),B).

power(B,s(E),R):-
              power(B,E,X),
              mul(X,B,R),!.

/**
Input: N
Output: Factorial
Funktion: Das Pr�dikat liefert eine SZahl, die SZahl ist ein Produkt der Fakult�t der zwei eingegebenen SZahlen.
Beschreibung des Angorithmus: Section 1.2.7 im Entwurf.
*/

fac(0,s(0)):- !.
fac(s(0),s(0)):- !.

fac(s(N), Factorial):-
                      fac(N,X),
                      mul(X,s(N),Factorial).
/**
Input: N1 und N2
Output: true oder false
Funktion: Wenn N1 == N2 return false, wenn N1 < N2 return true, wenn N1 > N2 return false.
Beschreibung des Angorithmus: Section 1.2.8 im Entwurf.
*/

lt(0,0):-
         !,false.

lt(0,_):-
         !,true.

lt(_,0):-
         !,false.

lt(s(N1), s(N2)):-
       lt(N1,N2).

/**
Input: N1 und N2
Output: true oder false
Funktion: wenn N1 == N2 return true, wenn N1 < N2 return false, wenn N1 > N2 return false.
*/

equals(0,0):-
             !,true.
equals(_,0):-
             !,false.
equals(0,_):-
             !,false.

equals(s(N1), s(N2)):-
              equals(N1,N2).

/**
Input: A und B
Output: C
Funktion: Das Pr�dikat liefert eine SZahl, die SZahl ist ein Rest der Division von den zwei eingegebenen SZahlen A und B.
Beschreibung des Angorithmus: Section 1.2.9 im Entwurf.
*/

mods(0,0,0):- !.

mods(A,s(0),A):-!.

mods(_,0,_):- !,false.

mods(A,B,0):- equals(A,B),!.

mods(A,B,A):- lt(A,B),!.

mods(A,B,C):-
             lt(B,A),
             sub(A,B,X),
             mods(X,B,C),!.
